﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUploadController.Models
{
    public class UploadResult
    {
        public string OriginalFileName { get; set; }
        public string IdOFile { get; set; }
        public string Type { get; set; }
        public bool Success { get; set; }
        public int FileIx { get; set; }
    }
}
