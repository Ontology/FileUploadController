﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUploadController.Models
{
    public class FileItem
    {
        public string originalName { get; set; }
        public string fileType { get; set; }
    }
}
