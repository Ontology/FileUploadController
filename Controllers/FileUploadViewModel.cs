﻿using FileUploadController.Models;
using FileUploadController.Notifications;
using MediaStore_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUploadController.Controllers
{
    public class FileUploadViewModel: OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.Navigation_IsSuccessful_Login);

            }
        }

        private UploadResult uploadResult;
        public UploadResult UploadResult
        {
            get { return uploadResult; }
            set
            {

                uploadResult = value;

                RaisePropertyChanged(NotifyChanges.Navigation_UploadResult);

            }
        }

        private ByteState bytestate_WriteState;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "writeState", ViewItemType = ViewItemType.Other)]
		public ByteState ByteState_WriteState
        {
            get { return bytestate_WriteState; }
            set
            {
                if (bytestate_WriteState == value) return;

                bytestate_WriteState = value;

                RaisePropertyChanged(nameof(ByteState_WriteState));

            }
        }

        private bool isenabled_Upload;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "uploadButton", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Upload
        {
            get { return isenabled_Upload; }
            set
            {
                if (isenabled_Upload == value) return;

                isenabled_Upload = value;

                RaisePropertyChanged(nameof(IsEnabled_Upload));

            }
        }

        private bool isenabled_FileChoose;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "filename", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_FileChoose
        {
            get { return isenabled_FileChoose; }
            set
            {
                if (isenabled_FileChoose == value) return;

                isenabled_FileChoose = value;

                RaisePropertyChanged(nameof(IsEnabled_FileChoose));

            }
        }
    }
}
