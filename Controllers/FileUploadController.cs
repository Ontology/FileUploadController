﻿using FileUploadController.Translations;
using OntoMsg_Module.Base;
using OntoMsg_Module.WebSocketServices;
using MediaStore_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Notifications;
using OntologyClasses.DataClasses;
using FileUploadController.Models;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace FileUploadController.Controllers
{
    public class FileUploadController : FileUploadViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;
        private TranslationController translationController = new TranslationController();
        private FileWorkManager fileWorkManager;

        private clsLogStates logStates = new clsLogStates();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        private List<UploadResult> uploadResults;
        private int fileIx;
        private int fileCount;

        public FileUploadController()
        {
            Initialize();
            PropertyChanged += FileUploadController_PropertyChanged;
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            fileWorkManager = new FileWorkManager();
            fileWorkManager.MediaStoreConnector.PropertyChanged += MediaStoreConnector_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            IsEnabled_FileChoose = true;
            IsEnabled_Upload = true;

            ByteState_WriteState = new ByteState();
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.FileUpload,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                IsEnabled_FileChoose = IsSuccessful_Login;
                IsEnabled_Upload = IsSuccessful_Login;

            }
        }

        private void MediaStoreConnector_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MediaStoreConnector.ByteState))
            {
                ByteState_WriteState = fileWorkManager.MediaStoreConnector.ByteState;

            }
        }

        private void FileUploadController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(UploadResult))
            {
                
            }

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.Navigation_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_IsReady_Upload ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_IsSuccessful_Upload ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_UploadResult ||
                e.PropertyName == nameof(ByteState_WriteState) ||
                e.PropertyName == nameof(IsEnabled_FileChoose) ||
                e.PropertyName == nameof(IsEnabled_Upload))
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            this.webSocketServiceAgent.receivedData += WebSocketServiceAgent_receivedData;

        }

        private void WebSocketServiceAgent_receivedData(byte[] byteBuffer)
        {
            IsEnabled_FileChoose = false;
            IsEnabled_Upload = false;
            ByteState_WriteState = new ByteState
            {
                ByteCount = 0,
                BytesDone = 0
            };
            
            var resultItem = uploadResults[fileIx];

            var result = fileWorkManager.CreateFile(resultItem.OriginalFileName);

            if (result.GUID == logStates.LogState_Error.GUID)
            {
                resultItem.Success = false;
                return;
            }
            resultItem.IdOFile = result.OList_Rel.First().GUID;
            result = fileWorkManager.MediaStoreConnector.SaveBytesToManagedMedia(result.OList_Rel.First(), byteBuffer);

            if (result.GUID == logStates.LogState_Success.GUID)
            {
                resultItem.Success = true;
            }
            else
            {
                resultItem.Success = false;
            }

            resultItem.Success = true;
            
            fileIx++;
            UploadResult = resultItem;
            if (fileIx == fileCount)
            {
                var oItems = uploadResults.Select(uploadResult => {
                    var oItem = fileWorkManager.GetFileItem(uploadResult.IdOFile, uploadResult.OriginalFileName);
                    oItem.Additional1 = uploadResult.Type;
                    return oItem;
                }).ToList();

                var interServiceMessage = new InterServiceMessage
                {
                    ChannelId = Notifications.NotifyChanges.Channel_FilesUploaded,
                    OItems = oItems
                };

                webSocketServiceAgent.SendInterModMessage(interServiceMessage);
            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                
                if (webSocketServiceAgent.Command_RequestedCommand == "FilesSelected" && webSocketServiceAgent.Request.ContainsKey("FileItems"))
                {
                    uploadResults = new List<UploadResult>();
                    List<FileItem> fileItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FileItem>>(webSocketServiceAgent.Request["FileItems"].ToString());
                    for (var ix = 0; ix < fileItems.Count; ix ++)
                    {
                        uploadResults.Add(new UploadResult
                        {
                            OriginalFileName = fileItems[ix].originalName,
                            Type = fileItems[ix].fileType,
                            FileIx = ix
                        });
                    }
                    fileCount = fileItems.Count;
                    fileIx = 0;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UploadFile")
                {
                    IsEnabled_FileChoose = false;
                    IsEnabled_Upload = false;
                    ByteState_WriteState.ByteCount = 100;
                    ByteState_WriteState.BytesDone = 1;
                }
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId != Channels.FileUpload) return;

            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }

}
